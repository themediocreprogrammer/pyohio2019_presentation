:auto-console: true
:title: The Mediocre Programmer
:data-transition-duration: 1500
:css: hovercraft.css

----

The Mediocre Programmer
=======================

by: Craig Maloney

Presented at PyOhio 2019

``http://decafbad.net``

----

Introduction
============

"Hi, I'm Craig Maloney"

----

There is a book
===============

.. image:: static/cover_ebook_1600x2400.png

``http://themediocreprogrammer.com``

.. note:: Send me your money

----

Why "The Mediocre Programmer"?

.. note:: Funny story about the real reason at the end of the presentation

----

How do you fly an airplane?

.. note:: Let's answer a quick question: how do you fly an airplane?

----

.. image:: static/7646064120_14ddfc5fb1_o.jpg

``https://flic.kr/p/cDE5tJ``

.. note:: Well, when you're younger this is how you might fly an airplane. Just get some paper, fold it a certain way and there you have it: an airplane

----

How do you fly a commercial airplane?

----

.. image:: static/4522259179_0fa1d2366a_o.jpg

``https://flic.kr/p/7TBL6R``

.. note:: Just turn these dials and flip thise switches here

----

.. image:: static/4522893700_97523c75eb_o.jpg

``https://flic.kr/p/7TF1HS``

.. note:: and watch these dials here and boom! You're flying.

----

.. image:: static/Good Luck.png

.. note:: And then the customer shows up...

----

**Note: I have no idea how to fly a commercial airplane**

----

What I mean by "Mediocre"

----

.. image:: static/fate_ladder.png

.. note:: This is the Fudge / Fate ladder from the Fudge / Fate roleplaying games. Mediocre is the modifier you get when you don't have any training in a particular skill.

----

``mediocre == unskilled``

----

.. image:: static/7646064120_14ddfc5fb1_o.jpg


Beginners learn basic concepts using a subset of skills on limited projects

----

.. code-block:: python

    Python 3.6.8 (default, Dec 25 2018, 00:00:00)
    [GCC 4.8.4] on linux
    Type "help", "copyright", "credits" or "license" for more information.
    >>> print("Hello world")
    Hello world
    >>> 2 + 2
    4

----

.. image:: static/Yaw_Axis_Corrected.svg

Advanced users have a wide range of skills learned by many hours of practice on complex systems.

``https://en.wikipedia.org/wiki/Aircraft_principal_axes``

----

What happens when you're no longer a beginner?

What happens when you're not quite advanced?

----

The distance between unskilled and skilled...

----

"The Gap"

.. note:: No, not a clothing store. This is my term for the distance between who we are at the moment and the person we want to be. Gaps form around us all of the time. We try to close these gaps by learning new things.

----

How do we close the gaps?

.. note:: Well...

----

*You can't close all of the gaps.*

----

You DO have options for how to deal with the gaps

----

Option #1
=========

Don't try to close the gaps

Stick with what you know, and let it ride

.. note:: This option isn't a good option, but it is the option that offers the least amount of work with the most amount of contentment.

----

Option #2
=========

Buy everything

Read everything

Watch everything

Do everything

.. note:: Get all the courses, buy all the books, read blog posts and become frustrated when you're not able to learn the totality of everything all at once. Option #2 is a great way to burnout and become frustrated.

----

Option #3
=========

Start with small tasks

Work toward clearly defined goals

Enjoy the journey

----

*(Guess which option I'm suggesting?)*

----

Option #1
=========

No progress

Option #2
=========

No hope of completion

Option #3
=========

Progress and small steps of completion

----

.. image:: static/1904_Will_&_Orv_at_HP.jpg

.. note:: The Wright Brothers took from 1896 to 1903 to get to where they could fly their first airplane. That's 7 years of tinkering, experimentation, frustration, mistakes, and failure. And that flight lasted 59 seconds and 852 ft. (which is 284 yards, or almost three football fields). But they kept trying and kept working at it. 

----

Becoming a better programmer is a slow and gradual process.

.. note:: I wish there were overnight solutions for becoming a better programmer. I know I would love to learn the totality of programming overnight, but unfortunately we don't have a good way of teaching people how to become better programmers quickly.

----

The metaphor of a journey

.. note:: I used the metaphor of a journey in the book because learning how to program is a journey. Everyone takes a different path and has different destinations. Programming is not a factory where beginners are deposited on one side and experts pop out the other. It's a winding road that we're carving out in our daily practice as programers. We'll have good travel days, and days when we make little to no progress.

----

If this is a journey then what do we want for our journey?

.. note:: We need to prepare for our journey. What do we want to take with us?

----

Good journeys require traveling companions

.. note:: If we think of programming as a journey we'll want to take along trusted companions to help guide us. They don't necessarily need to be with us in person all the time. They can be the programmers that wrote books for us to read or programmers in online communities. We just need to have folks who can help us along in our journey.

----

All programmers are your peers

.. note:: We're all peers when we're developing. Sure there are programmers with vastly more skill and knowledge than you or I might have (or ever have) but we're all in the same profession. If you think of others as peers then we start to strip away some of the fear of asking questions and advice from other programmers.

----

Leverage the talents of others

.. note:: Once we realize that we're not competing we can instead leverage the talents of one another. We all have talents and things that interest us, whether that's web development, embedded development, design; whatever it is that tickles our fancy we can find ourselves companions that can help us along on our journey.

----

*The book has advice on finding a good community*

----

Traps along the way...
======================

(Here be dragons)

----

**Be careful**

Backstage vs. Performance
=========================

Comparing our rehearsal to others highlight reels

.. note:: We're unreliable narrators of our experiences. We tend to edit out the not-terribly-interesting parts of our experiences. We highlight the parts that worked out well, and (if it's not terribly embarassing) we tell only the spectacular failures that are interesting or useful. When we compare ourselves with others we get their story of what they believe their experiences were, not the whole story. That can be frustrating when we look at our own experiences and struggles against the slick production of others' code.

----

**Be careful**

Online ranking systems
======================

.. note:: Another trap is looking at online ranking systems like Hacker Rank. Any time you put a number to something you turn it into a game. So we make contests out of things like number of lines of code to solve a problem, numbers of problems solved, etc. All these do is show whether someone has put in the time to get better at these sorts of problems. These numbers are meaningless. It's akin to grading folks on how many crossword puzzles they can do in an afternoon: in the end you only know their skill in working with crossword puzzles and that they made the time to work on crossword puzzles.

----

The M word (Mistakes)

.. note:: I'd like to talk about the M word. It's a word that no programmer wants to hear or admit to ever having done.  Mistakes are something that programmers have to deal with. It's something we don't give enough attention to, or view as something positive.

----

You will make mistakes

.. note:: The fact is that we all make mistakes, and will continue to make mistakes. Mistakes are part of how we learn. We can't help but make mistakes as we're programming.

----

The fear of making mistakes

.. note:: The problem comes when we start being afraid of making mistakes. We fret about learning a language wrong, or worry about "breaking the build" or having tests pass that shouldn't pass. We're always looking over our shoulder to make sure that our record is clean and that when a mistake is discovered that we're not the ones blamed for the mistake. (Scale of mistakes comes later, so don't riff on that here).

----

How to make the best mistakes

(a primer)

.. note::  We need mistakes to help us learn and grow. Mistakes are how we learn what doesn't work so we can be better able to find what does work. You need somewhere safe that you can use to make mistakes.

----

Making a model

.. note:: What we need is a place where we can make mistakes with as few consequences as possible. If the stakes are too high then we'll do everything to avoid making any mistakes. But that means we won't experiment either for fear of making those mistakes. If we can make a model of our production systems where the consequences form making mistakes are minimized then we have the ability to grow. This can be anything from virtual environments on development machines to testing labs. But there's also a question of the fidelity of the model...

----

.. image:: static/61u7-fvQkCL._SX425_.jpg
.. image:: static/42305421334_a6442eb1f4_z.jpg

.. note:: We also need to determine the fidelity of our model. The Lego model looks like a Sopwith Camel, but if we need to determine how well a Sopwith Camel flies the it won't work. So we need to determine the bare minimum for our models so we can use them effectively. (model image: https://flic.kr/p/27sokS7)

----

.. image:: static/snoopy_sopwith_camel.jpg

.. note:: You want your movel to be effective enough that you can use it to make mistakes with the lowest penalty for making those mistakes. Then you too can be w flying ace.

----

Time machines

.. note:: Another way we can help mitigate our mistakes is by having a way to roll back time. Version control is the most common, but even development environments that allow us to take a snapshot of and roll back history can be helpful. This also takes some discipline on our part to use these tools. That means frequent commits and snapshots.

----

Mistakes and practice are part of the learning process

.. note:: Good models and ways to roll-back are key to helping you make and recover from mistakes.  

----

Fear and uncertainty
====================

(Recurring characters in this tale)

.. note:: A lot of programming comes with overcoming fear and uncertainty, especially the fear and uncertainty of failure. We fear that we're not good enough or that we'll let others down. We're uncertain if things will work out the way we hope. But if we give ourselves areas where we can practice we can gain more confidence in our abilities. Also whenever I find myself in a situation where I am afraid and uncertain then I know there's a gap nearby that I'll want to work on. 

----

How to get better at learning
=============================

Keep a journal

.. note:: I don't do this as much as I should, but it's a key component for our learning. Writing things down (whether that's in a text file, a notebook, or whatever) is how you best remember what you did. Remembering all of this stuff off the cuff is easy at first when there isn't that much to remember, but later on it gets harder to recall things. By writing things down you give your mind the freedom to let go of being the primary source for this information. Usually you'll remember where you wrote it down faster than you'll remember what the details are. So give yourself a journaling habit and experiment with different types of journaling to see which one fits your work and lifestyle. (And make it fun too!)

----

How to get better at learning
=============================

Share your successes and failures

.. note:: We can help reinforce our learning by sharing our knowledge with others, whether that is via blog posts, presentations, writing books, talking with a friend over our favorite beverages, or whatever means we use. When we communicate with others it's similar to journaling in that we are organizing our thoughts to present them to someone else. We're telling a story, and that is something we've learned to do over the years. By sharing our successes and failures we can help others who are dealing with the same issues that we're facing, and give them some ideas on what worked and what didn't. And they too can share their stories with us, which can lead to us learning more and growing.

----

Programming is not a solo activity

.. note:: We tend to think of programming as a solo actvity. And yes, programming requires a lot of solitude in order to be most effective in our craft. Lord knows I've been in offices where I've used loud angry technical death metal to ensure that I have the solitude that I need in order to program. What I mean though is that the act of programming isn't just about us. We need others to help us along the way. And I'm not talking just about others to creat compilers, editors, and operating systems; I mean that we need the support of others during our programming craft.

----

Practicing with Fear and Discomfort
===================================

.. note:: Now that we know about how to share our experiences with others let's talk a bit about practicing with the fear and discomfort while programming

----

One thing that has helped me...

.. note:: I want  to share one thing that has helped me along the way. It helped me write this book (at least 10 minutes a day) ahd has helped me with my learning practice.

----

Containers

.. note:: What I have found that works is containers. A container is a self-contained period of time where you agree to work on a task for a specified amount of time. This can be as short as 5 minutes, or as long as 30 minutes (or 25 minutes if you're doing The Pomodoro Technique (R)).

----

Start with the intention

.. note:: At the beginning of the container you start with your intention. What do you want to be true at the end of the container. Ideally it's something that can be completed by the end of the container. Make it a simple and tangible goal. Saying "I want to work on this project for 10 minutes" is better than "I want to have this piece of code debugged and fully tested in 10 minutes"

----

Close out everything but this task

.. note:: Maximize this window and work in full-screen mode. There are many things that are designed to get your attention. Turn those off as much as you are able. I'd recommend exiting your email client and setting as many devices to "do not disturb" during this time. For now all that exists is you and your work.

----

Set the timer

.. note:: Once you have the intention then you set the timer. I sugges starting with 10 minutes, but anywhere from 5-25 minutes works fine. For me 10 minutes is the optimal time for me to get my mind to be quiet and focus on the work.

----

Start the timer and begin the work

.. note:: Once the timer starts you are fully focused in the work. You're not checking email or shifting to other tasks; you're just working through your intended task.

----

Be present with the work

.. note:: The work may be challenging and you might want to run away from it. That's normal. What you want to do is notice when this happens and either write that down on a piece of paper, or note that in a journaling application. You may also find yourself thinking about other things that you can be working on at that moment. Again, write those down so you don't forget about them and then return to the work. Eventually your mind will allow you to fully focus on the task at hand, but you might encounter resistance as you work. That's normal. Sometimes our minds are so full of other things that we get sidetracked with all of the possibilities of what we could be doing that we don't focus on what we are doing.

----

Distractions

.. note:: If you're like most folks you'll probably have telephone calls, instant messages, texts, or email flowing in to try to distract you. Notice those distractions and the tendencies to run away from the work and them move your focus back to your work.

----

Time's up!

.. note:: Once the timer ends we then close out the work as much as we can. This is a good time to hit save. Take a deep breath and relax. You can even have a bit of gratitude that you are now however many minutes further along in your journey.

----

Take the break

.. note:: Give yourself an opportunity to take a break if you need one. If you need to bring up your email / IM applications and catch up on what happened in the interim.

----

How to take a break
===================

(a short course)

.. note:: So, how do we best take a break?

----

Move away from the desk

.. note:: I know my tendency is to move over to another application (email, chat, social media) but that's not taking a break; that's bad multitasking. The best way to take a break is to move away from the desk.

----

If you can't move from your desk look away from the computer for a bit

.. note:: Give yourself a context switch. Whether that's to take your preferred bio break, or just go outside, you need to have a situation where your not looking at your workspace. But look at it as a bit of a reward for your work, not as an interruption.

----

Breaks are not interruptions
============================

.. note:: If you view your breaks as interruptions in your work then you'll be more reluctant to take them. Consider them part of your work and take the breaks.

----

More on distractions
====================

The fear of missing out

.. note:: I think part of why we're so attached to email, social media, IM, and chat programs is our fear of missing out. If we're not always connected then we might not be available if soemone needs us, or we'll need to spend additional time catching up on the news and events of the day. One of the ways we can help ourselves is by taking containers like this to really focus in on our work and be present with it. Giving ourselves these hits of focus will help us exercise our focus and help us be more resilliant to resisting distractions.

----

Programming requires flexibility

.. note:: What worked for us five years ago might not still be working today. In 2012 the Raspberry Pi was introduced. That was seven years ago. In the 2010s we've seen the release of Rust, Elm, Elixir, TypeScript, Kotlin, and Reason. We'll probably see more before this decade is out. We need to be flexible in what we learn and what we decide is no longer relevant to our work.

----

Programmer Flexibility
======================

Learning how to learn

.. note:: One of the best things we can do as programmers is learn how we learn. Some of us can sit down with some videos and learn the basics of something with no difficulty. Others need repetition and reinforcement. I need to have a constanti daily practice in order to learn effectively. That's where the containers help me. With a container and daily repetition I was able to learn more effectively than when I was trying to schedule a few hours here and there.

----

The Learning Process
====================

Choosing what to learn

.. note:: Make a list of the things that you want to learn and then decide from that list. If you're having trouble deciding then roll a die (or if you're feeling really clever write a program to pick one from that list)

----

Focus:
======

Learn one thing at a time

.. note:: We need to learn things one at a time. Resist the urge to learn everything at the same time. Give youself the ability to focus on learning one thing at a time and be OK with not learning the rest for now.

----

Focus:
======

Use one learning tool at a time

Use that tool for a week

.. note:: Do a quick search on some learning tools and then use it for a weel to learn.

----

The goal is learning, not building a library
============================================

Comparison shopping is not programming

.. note:: I think I should have this slide framed. The goal of all of this is to learn. You're not building a library. This shouldn't be a shrine to your ability to accumulate books, videos, and courses. You'll find some good ones and some duds. That's OK. Even the bad ones will teach you something (even if that's how to pick better material). You haven't wasted your time if you pick bad material.

----

Discomfort while learning

.. note:: Any time we learn new things we put ourselves into a vulnerable and uncomfortable place. We take the things we are familiar with and try to apply them as we push into new territory. We become uncertain of the outcome; will it be successful or will it be a failure? Will this topic be too difficult for us to grasp?  Will we choose the wrong thing to learn and will that cost us opportunities in the long run? Discomfort is natural and part of the process of learning. Be OK with that discomfort.

----

Use containers to practice with fear and discomfort

.. note:: This is why the focus container that we mentioned before is so important: it gives us small doses of discomfort and difficulty in manageable chunks, and an area to practice with that discomfort. We can handle discomfort if we know that it will end soon.

----

Revelation
==========

All programming is connected

.. note:: As you progress with learning you'll start to see that a lot of what we call programming is interconnected. Languages borrow heavily from each other and ideas that seem new and innovative have their roots in concepts dating back to the genesis of computing. But rather than dissuade us it should encourage us. We can open the doors of programming by learning simple, transferable concepts.

----

Programming Languages
=====================

What are their influences?

.. note:: A common question of musicians is "what are your influences?". This usually results in the musician listing off a lot of artists (some of which you might have heard of, most you might not). One thing that has expanded my musical vocabulary is following those influences. If Webb Wilder says he was influenced by Big Joe Williams, I'll see if I can find any recordings by Big Joe Williams. Sometimes bands will cover songs that influenced them, which is how a lot of metalheads learned about Joe Jackson when Anthrax covered "Got the Time". Languages also have influences. many of the languages you use today were inspired and influenced by other languages. I read that JavaScript borrowed heavily from Scheme so I decided to take a peek and see what was in Scheme. This helped me understand more about functional programming and gave me insight into how JavaScript works. 

----

Be curious

.. note:: As beginners we engaged the computer with curiosity and enthusiasm. We didn't know what to expect and had no idea how long it would take. As our programming matured we traded our curiosity for certainty, and our enthusiasm for expectations. The excitement we got from learning became drudgery. But we can re-capture that beginner's spirit by understanding that each opportunity to learn as a new experience. We can drop our expectations of how things should be and instead approach our learning with curiosity. That will help us re-kindle the spark we had when we were beginners with infinite possibilities. 

----

Emotions
========

.. note:: We don't tend to talk about emotions that arise while we're programming. The stereotype is that we're emotionless programmers who just quietly tap in code into the computer as though we're in a trance. But if you've ever programmed you'll know it's anything but emotionless. We're like frantic conductors who swell when things are good, despair when things are terrible, and swear like sailors when everything goes horribly wrong.

----

Programming can be emotionally draining

.. note:: Programming is a taxing process. Not only do we need to keep a mental model of the software we're working on, but we also keep a mental model of how the software should behave. Our emotional state can mirror what we feel about what we're creating; whether we're excited, bored, or stuck. Keeping a positive attitude about software that isn't measuring up to our expectations is exhausting. Couple that with our own insecurities, fears, and doubts and you begin to understand why programmers tend to burn out.

----

Things that can affect our emotions...

.. note:: There are several factors that can cause us emotional highs and lows while programming. These are some that I've noticed in my own programming.

----

Purpose and utility

.. note:: What's the purpose of what we're doing? Is this going to be useful? If we're working on something that has meaning to us then we're going to want to keep working on it. But if we don't see the point or if it's not useful then we can feel like this is a waste of our time.

----

Engagement vs. boredom

.. note:: Are we engaged or are we bored? Are our synapses firing with delight or are we just going through the motions. 

----

Awake vs. tired

.. note:: This is  a big one. Am I awake and ready to go or am I so tired that dragging myself in front of the computer is all I can do?

----

Awareness of our Emotional and Mental state

(Emotional triage)

.. note:: Being aware of our current emotional and mental state can give us guidance on what we can do to move forward. I'm using mental state here to cover a lot of different mental factors, whether that's engagement, burnout, PTSD, or any other medical factors that may come into play. Programming takes a lot of mental bandwidth. We need to be aware of what might be interfering with that bandwidth. Think of it as emotional triage where we're not trying to make things different but rather understand where we are at this point and how we got to this emotional state.

----

Our story

.. note:: Each one of us has a story that we tell ourselves. These stories shape our perception of the world. Our emotions help inform the type of story we tell. If we're feeling amazing we tell ourselves that what lies ahead will also be amazing. If we're feeling down and defeated our story reflects our defeated tone.

----

Our stories are just stories

.. note:: The truth is that our story is just that: a story. Our stories are not a guarantee of how the day will progress. 

----

Tell yourself a better story
============================

(The gift of focusing on the present)

.. note:: Focusing on the present and the next steps we're taking gives us the freedom to re-calibrate as the day progresses. We can focus on the positive aspects of what we're doing instead of fretting about how reality is diverging from our stories. Even something simple as "my computer booted this morning" can be something positive (and sometimes might even be a miracle). We can course-correct throughout the day and navigate towards a more productive day.

----

Burnout
=======

.. note:: One thing that our emotional triage can help us diagnose is feeling burned out. Burnout can be something as simple as being bored or overworked but it can also be the sign of something more serious. Burnout can lead to physical or mental complications if we're not careful. We can work ourselves into serious levels of exhaustion and delude ourselves into thinking it's part of the price we have to pay as programmers. 

----

Burnout is a collection of seemingly unrelated emotions

.. note:: Burnout is tricky to self-diagnose because it is a collection of seemingly unrelated emotions. Our feelings of boredom, fear, exhaustion, and anxiety can all have different root causes, but when we combine them with an unrelenting working schedule and loss of control we amplify those feelings. Left unchecked we will try to numb out those feelings. We'll want to stop programming, and resent ourselves for getting into programming in the first place. We can cause ourselves more undue suffering by just "powering through it" which can lead us to compound and complicate our emotional state.

----

How to alleviate burnout...
===========================

----

Realize that we're burned out

(or about to burn out)


.. note:: Acknowledging that we're about to burn out is key to not experiencing the burnout. That seems simple enough but we tend to ignore the symptoms when we're nearing the throes of burnout. If we can recognize that we're about to burn out then we can take measures to avoid it. And if we realize that we're burned out we can take measures to be kind to ourselves and help ourselves through this burned-out state.

----

Examine our emotions

.. note:: Sit quietly for a while and feel what emotions come into your mind. Are we feeling stress, fear, anxiety, nervousness, or anger? Notice what feelings emerge and recognize these feelings. Examine where these feelings are coming from and what might be triggering these emotions.

----

Re-negotiate our commitments.

.. note:: Many times burnout is the result of over commitment, whether to ourselves or others. We always have too much to do, and worry we won't meet our obligations. Perhaps the plans we made were too aggressive, or something changed in the world that disrupted our plans. Whatever the reasons we may need to re-evaluate what is expected of us and what we are capable of doing. If we see that we've created an intractable situation for ourselves we need to figure out how to cut away some of these obligations or re-negotiate them.

----

Give our "drive" a rest.

.. note:: Give our "drive" a rest. Unlike our mechanical counterparts we need downtime and rest. We can't work a straight 8 or more hours without at least some moments where we aren't working. Programming demands a lot of mental bandwidth and pushing ourselves to exhaustion can lead to emotional instability, stress, and burnout.

----

Examine if this is truly how we want to live our lives.

.. note:: We need to be aware if what we're doing is really what we want to be doing. If we're not happy doing this then every moment we continue doing this can compound our feelings of unhappiness. If we look deep in ourselves and feel nothing but dread for our current situation then we may need to renegotiate our commitments. That can be something as simple as agreeing not to learn something right now, or can be as complex as taking on different work or changing careers.

----

Reaching out for help

.. note:: It's OK to ask others for help. I've struggled with asking for help. Part of my reluctance was getting the dreaded "you should know that already" response. Other times I thought asking for help would diminish my reputation. I'd be exposed as a fraud and an impostor. But when I did ask for help the response I received wasn't "why don't you know this?"; it was "why didn't you ask for help sooner?". The benefits of asking for help outweighed the negative effects I might face.

----

Help doesn't just mean technical help

.. note:: Asking for help isn't limited to just asking technical questions. There are many more ways that we might need help. This may involve folks like our colleagues, but it may also mean asking our management for support in difficult times, asking friends and loved ones for help, or even a whole other set of support staff such as doctors, therapists, and so on. 

----

Reluctance in asking for help

.. note:: Asking for help places us in a position where we're uncertain of the outcome and how others will perceive us. We run away from that discomfort even though asking for help may help us with even greater fear and danger. By embracing this discomfort we can get the help that we need so we can do our best work.

----

Asking for help is a skill

(and skills are something we can practice)

.. note:: Asking for and receiving help is a skill and like any skill it needs practice. When we're young we have simple means of asking for help (crying, pointing, etc.). But as we grow our world becomes more complex. Our methods for asking for help need to mature as we mature. This is not something that comes naturally to any of us. We struggle when we ask for help, and resist when others try to help us. But with repetition and careful practice we can improve these skills.

----

Giving up
=========

(Quitting)

.. note:: None of us likes to think about quitting programming. For some of us it's what we've wanted to do all of our lives. I've known that I wanted to be a programmer since I knew there were computers. But there's been many times where I've wondered if one of the career tests I took was right: maybe I should be a farmer.

----

Programming is more than the job market

.. note:: There's a tendency to comflate our worth in programming to what someone will pay us to do it. That's why people still ask if COBOL would be a useful to learn. If you only learn what is marketable then you'll find a very narrow subset of programming languages and skills. And frankly they're not terribly interesting. Sure, companies are adopting cool things like machine learning and natural language processing, but eventually those technologies will become boring and mundane as businesses decide what parts are valuable to them and ignore the rest.

----

Bringing joy back to programming

.. note:: Part of the joy of programming is the curiosity. If we can tap into that curiosity then we have so many avenues to explore. There are always topics and ideas to discover, including areas like game development, esoteric languages, or other programming paradigms. What the job market uses is a fraction of what is out there waiting to be explored. There's also a whole host of emulators and retro-computers available with good documentation and vibrant communities. I've been intrigued by learning how 8-bit computers work. 8-bit computers are simple and can learned rather easily as they are well-understood technology and older programs usually had only one programmer.

----

When the spark really is gone

.. note:: If we no longer find joy in programming then we need to understand why we feel that way. Perhaps we're tired and have been through a project that sapped the fun and excitement of programming for us. Or we've found that the communities online and in our area are hostile and unwelcoming. Maybe we thought programming would be fun but every time we start we wish we were doing something / anything else instead. Programming is not for everyone. Programming is something that is best when you really want to do it. If you're stuck in a situation where you don't want to do this anymore then it's perfectly reasonable to step away from it and give up. There's no shame in this at all.

----

Programming is only one facet of our lives

.. note:: Programming is only one facet of our lives. True, it may be a big facet of our lives, and it may feel scary to give up something that we've worked so hard to accomplish. But if we examine our feelings and realize that we're just going through the motions, or find that we're no longer experiencing any joy in programming then it's time to think about what else we can be doing with our lives outside of programming. We're granted a limited amount of time to live our lives and doing something we don't enjoy robs us of a meaningful life.

----

It's OK to stop being a programmer

(temporarily or permanently)

.. note::  Whether or not you make that a permanent change is up to you and your desires. Feeling emotionally drained, uninspired, and burned out is counterproductive to your programming practice --- programming is hard enough. Taking a break from programming to explore other interests is natural and doesn't mean you're less of a programmer for wanting to do something different to recharge yourself. If you find that you're happiest when you're not programming then pursue whatever else has your attention with wild abandon. Remember: our lives take many different turns and paths. The best path for you is the one you make yourself, regardless of where that might lead.

----

*(I hope you'll continue, but that's entirely up to you)*

----

Your journey is unique

.. note:: Every programmer's journey is unique. Your journey is going to wend and wind in directions that are different than the directions that my journey took. You'll have experiences that I won't share, and I've had experiences that will be difficult for you to replicate (unless you have a time machine). But neither of our experiences is more or less valid than each other, nor are they more or less valid than the experiences of other programmers. They are just our experiences. The areas of our knowledge are just the areas that we've explored so far. There will always be gaps, but those are only areas that we haven't explored yet.

----

I wish you well on your journeys, and hope to hear the tales of your travels when we meet again.

----

Until we meet again...

----

Thank you!

----

There is a book
===============

.. image:: static/cover_ebook_1600x2400.png

``http://themediocreprogrammer.com``

.. note:: Send me your money
